package com.datumsoft.app.mrsk_consumer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.loaders.FeatureListLoader;
import com.datumsoft.app.mrsk_consumer.models.ForeignField;
import com.datumsoft.app.mrsk_consumer.models.ISUConsumer;
import com.datumsoft.app.mrsk_consumer.models.ISUConsumerListDeserializer;
import com.datumsoft.app.mrsk_consumer.models.ISUCounter;

import org.osmdroid.util.BoundingBoxE6;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConsumerListActivity extends AppCompatActivity {

    public static String EXTRA_CONSUMER_POINT =
            "CONSUMER_POINT";

    private static String EXTRA_MAP_BBOX =
            "MAP_BBOX";

    private static String EXTRA_LOGIN =
            "LOGIN";

    private RecyclerView mConsumerRecyclerView;

    private ConsumerListAdapter mAdapter;
    private ArrayList<ISUConsumer> mConsumers;
    private ProgressBar mProgressBar;
    private int mLoginID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumer_list);
        mLoginID = getIntent().getIntExtra(EXTRA_LOGIN, 0);

        mConsumerRecyclerView = (RecyclerView) findViewById(R.id.consumer_recycler_view);
        mProgressBar = (ProgressBar) findViewById(R.id.consumer_list_progress);
        BoundingBoxE6 mapBbox = (BoundingBoxE6) getIntent().getSerializableExtra(EXTRA_MAP_BBOX);
        new ConsumerListTask(this, mapBbox).execute();
    }

    public static Intent newIntent(Context context, BoundingBoxE6 mapBbox, int loginID) {
        Intent intent = new Intent(context, ConsumerListActivity.class);
        intent.putExtra(EXTRA_MAP_BBOX, (Serializable) mapBbox);
        intent.putExtra(EXTRA_LOGIN, loginID);
        return intent;
    }

    class ConsumerListTask extends AsyncTask<Void, Void, ArrayList<ISUConsumer>>
    {

        private Activity mActivity;
        private BoundingBoxE6 mBBOX;

        public ConsumerListTask(Activity activity, BoundingBoxE6 bbox){
            mActivity = activity;
            mBBOX = bbox;
        }

        @Override
        protected ArrayList<ISUConsumer> doInBackground(Void... params) {
            FeatureListLoader<ISUConsumer> consumerLoader = new FeatureListLoader<>(mActivity.getResources().getString(R.string.rest_point),
                    ISUConsumer.FEATURE_CLASS, ISUConsumer.class, new ISUConsumerListDeserializer());
            consumerLoader.addFilterOption("geom__isnull", false);
            consumerLoader.addFilterOption("geom__intersects", Utils.BboxToWkt(mBBOX));
            ArrayList<ISUConsumer> consumers = consumerLoader.load();
            return consumers;
        }

        @Override
        protected void onPostExecute(ArrayList<ISUConsumer> isuConsumers) {
            mConsumers = isuConsumers;
            mAdapter =  new ConsumerListAdapter(mActivity, mConsumers, mLoginID);
            mProgressBar.setVisibility(ProgressBar.GONE);
            mConsumerRecyclerView.setAdapter(mAdapter);
            mConsumerRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

}

class ConsumerListAdapter extends RecyclerView.Adapter<ConsumerListAdapter.ViewHolder>
{
    private List<ISUConsumer> mConsumerDataset;
    private Activity mActivity;
    private int mLoginID;

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener
    {
        private ISUConsumer mConsumer;
        private Context mContext;
        private TextView mCodeTextView;
        private TextView mNameTextView;
        private TextView mAddressTextView;
        private ImageButton mLocationButton;


        public ViewHolder(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);
            mContext = itemView.getContext();
            mCodeTextView = (TextView) itemView.findViewById(R.id.consumer_code_view);
            mNameTextView = (TextView) itemView.findViewById(R.id.consumer_name_view);
            mAddressTextView = (TextView) itemView.findViewById(R.id.consumer_address_view);
            mLocationButton = (ImageButton) itemView.findViewById(R.id.consumer_location_btn);
            mLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent data = new Intent();
                    data.putExtra(ConsumerListActivity.EXTRA_CONSUMER_POINT, (Serializable)mConsumer.getPoint());
                    mActivity.setResult(Activity.RESULT_OK, data);
                    mActivity.finish();
                }
            });
        }

        public void setConsumer(ISUConsumer consumer) {
            mConsumer = consumer;
            mCodeTextView.setText(mConsumer.getCode());
            mNameTextView.setText(mConsumer.getName());
            mAddressTextView.setText(mConsumer.getAddress());
        }

        @Override
        public void onClick(View v) {
            Intent intent = ConsumerActivity.newIntent(mContext, mConsumer, mLoginID);
            mContext.startActivity(intent);
        }
    }

    public ConsumerListAdapter(Activity activity, List<ISUConsumer> consumerDataset,
                               int loginID){
        mActivity = activity;
        mConsumerDataset = consumerDataset;
        mLoginID = loginID;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_consumer_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setConsumer(mConsumerDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mConsumerDataset.size();
    }
}