package com.datumsoft.app.mrsk_consumer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.loaders.CounterListLoader;
import com.datumsoft.app.mrsk_consumer.models.ISUConsumer;
import com.datumsoft.app.mrsk_consumer.models.ISUCounter;

import java.util.ArrayList;

public class ConsumerActivity extends AppCompatActivity {

    private static final String EXTRA_CONSUMER =
            "consumer";

    private static final String EXTRA_LOGIN =
            "LOGIN";

    ISUConsumer consumer;
    TableLayout tlAttributes;
    int mLoginID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumer);

        Intent intent = getIntent();
        consumer =  (ISUConsumer) intent.getSerializableExtra(EXTRA_CONSUMER);
        mLoginID = getIntent().getIntExtra(EXTRA_LOGIN, 0);

        tlAttributes = (TableLayout) findViewById(R.id.tlAttributes);
        showConsumerAttrs();
    }

    public static Intent newIntent(Context context, ISUConsumer consumer, int loginID){
        Intent intent = new Intent(context, ConsumerActivity.class);
        intent.putExtra(EXTRA_CONSUMER, consumer);
        intent.putExtra(EXTRA_LOGIN, loginID);
        return intent;
    }

    private void showConsumerAttrs(){
        if (consumer == null){
            return;
        }
        appendRowToTemplateAttrTable("Номер договора", consumer.getCode());
        appendRowToTemplateAttrTable("Наименование организации", consumer.getName());
        appendRowToTemplateAttrTable("Наименование договора", consumer.getNameContract());
        appendRowToTemplateAttrTable("Серийный номер", consumer.getSerialNumber());

        appendRowToTemplateAttrTable("РСК", consumer.getRsk().toString());
        appendRowToTemplateAttrTable("ПЭС", consumer.getPes().toString());
        appendRowToTemplateAttrTable("РЭС", consumer.getRes().toString());

        appendRowToTemplateAttrTable("ККС", consumer.getKks());
        appendRowToTemplateAttrTable("Город", consumer.getCity());
        appendRowToTemplateAttrTable("Улица", consumer.getStreet());
        appendRowToTemplateAttrTable("Дом", consumer.getHouse());
    }


    private void appendRowToTemplateAttrTable(String caption, String value){
        LayoutInflater inflater = LayoutInflater.from(this);
        View rowView = inflater.inflate(R.layout.fragment_attribute_table_row, null);
        ((TextView)rowView.findViewById(R.id.caption_row_attr)).setText(caption);
        ((TextView)rowView.findViewById(R.id.value_row_attr)).setText(value);
        tlAttributes.addView(rowView);

    }


    private void appendRowToAttrTable(String caption, String value){
        TableRow row = new TableRow(this);

        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);

        LinearLayout.LayoutParams captionViewLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        TextView captionView = new TextView(this);
//        captionView.setLayoutParams(captionViewLayoutParams);
        captionView.setText(caption);
        captionView.setMaxLines(2);
        row.addView(captionView);


        LinearLayout.LayoutParams valueViewLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        TextView valueView = new TextView(this);
//        valueView.setLayoutParams(valueViewLayoutParams);
        valueView.setSingleLine(false);
        valueView.setText(value);
        valueView.setMaxLines(3);
        valueView.setLines(2);
        valueView.setEllipsize(null);
        valueView.setHorizontallyScrolling(false);
        valueView.setTextAppearance(this, android.R.style.TextAppearance_Medium);
        row.addView(valueView);

        tlAttributes.addView(row);
    }

    public void onClickCounterList(View v){
        new LoadingCounterListDataAsyncTask(ConsumerActivity.this).execute();
    }

    private class LoadingCounterListDataAsyncTask extends AsyncTask<String, Void, Void>
    {
        private Context mContext;
        private ArrayList<ISUCounter> mCounterList;

        public LoadingCounterListDataAsyncTask(Context context){
            super();
            mContext = context;
        }

        @Override
        protected Void doInBackground(String... params) {
            CounterListLoader loader = new CounterListLoader(getResources().getString(R.string.rest_point));
            loader.addFilterOption("consumer", consumer.getCode());
            mCounterList = loader.load();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent = CounterListActivity.newIntent(mContext, mCounterList, consumer.getCode(), mLoginID);
            startActivity(intent);
        }
    }
}
