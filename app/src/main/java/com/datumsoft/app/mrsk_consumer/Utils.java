package com.datumsoft.app.mrsk_consumer;

import android.support.annotation.NonNull;
import android.util.Log;

import org.osmdroid.util.BoundingBoxE6;

import java.util.Locale;

/**
 * Created by godko on 31.08.2016.
 */
public final class Utils {

    private Utils(){

    }

    @NonNull
    public static String BboxToWkt(BoundingBoxE6 bbox){
        double latNorth = ConvertECoordToDouble(bbox.getLatNorthE6()),
                latSouth = ConvertECoordToDouble(bbox.getLatSouthE6()),
                lonEast = ConvertECoordToDouble(bbox.getLonEastE6()),
                lonWest = ConvertECoordToDouble(bbox.getLonWestE6());
        Log.d("map", "testing bbox");
        String wktFormat = "POLYGON((%1$f %2$f, %1$f %3$f, %4$f %3$f, %4$f %2$f, %1$f %2$f))";
        return String.format(Locale.ROOT, wktFormat, lonEast, latNorth, latSouth, lonWest);
    }

    private static double ConvertECoordToDouble(int coord){
        return coord / 1E6;
    }
}
