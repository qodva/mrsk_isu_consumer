package com.datumsoft.app.mrsk_consumer.models;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.osmdroid.util.GeoPoint;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by godko on 19.09.2016.
 */
public class ISUConsumerListDeserializer implements JsonDeserializer<ArrayList<ISUConsumer>> {

    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    private final String TAG = "ISUCounterDeserializer";
    private static Pattern sPattern = Pattern.compile("POINT\\s\\((\\d*\\.?\\d*)\\s*(\\d*\\.?\\d*)\\)");

    @Override
    public ArrayList<ISUConsumer> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ArrayList<ISUConsumer> counterCollection = new ArrayList<>();
        JsonObject obj = json.getAsJsonObject();
        JsonArray results = obj.getAsJsonArray("results");
        if (results.size() > 0){
            for (JsonElement el: results){
                counterCollection.add(parseConsumer(el.getAsJsonObject()));
            }
        }
        return counterCollection;
    }

    private ISUConsumer parseConsumer(JsonObject obj){
        ISUConsumer consumer = new ISUConsumer();
        consumer.setDescription(obj.get("__unicode__").getAsString());
        consumer.setCode(obj.get("pk").getAsString());
        JsonObject fields = obj.getAsJsonObject("fields");
        JsonElement el = null;

        consumer.setRsk(ForeignField.FromJsonObject(fields.getAsJsonObject("rsk")));
        consumer.setPes(ForeignField.FromJsonObject(fields.getAsJsonObject("pes")));
        consumer.setRes(ForeignField.FromJsonObject(fields.getAsJsonObject("res")));

        consumer.setKks(fields.get("kks").getAsString());
        consumer.setName(fields.get("name").getAsString());
        consumer.setAddress(fields.get("address").getAsString());
        consumer.setCity(fields.get("city").getAsString());
        consumer.setStreet(fields.get("street").getAsString());
        consumer.setHouse(fields.get("house").getAsString());
        consumer.setSerialNumber(fields.get("serial_number").getAsString());
        if (fields.has("name_contract")){
            el = fields.get("name_contract");
            if (!el.isJsonNull()){
                consumer.setNameContract(fields.get("name_contract").getAsString());
            }
        }
        if (fields.has("geom")){
            consumer.setPoint(buildGeoPoint(fields.get("geom").getAsString()));
        }
        consumer.setCounterType(fields.get("counter_type").getAsString());
        consumer.setLegal(fields.get("is_legal").getAsBoolean());

        return consumer;
    }

    private GeoPoint buildGeoPoint(String geoString){
        Matcher matcher = sPattern.matcher(geoString);
        GeoPoint point = null;
        if (matcher.find() && matcher.groupCount() == 2){
            double lat = Double.parseDouble(matcher.group(2));
            double lon = Double.parseDouble(matcher.group(1));
            point = new GeoPoint(lat, lon);
        }
        return point;
    }
}
