package com.datumsoft.app.mrsk_consumer.models;

/**
 * Created by godko on 26.09.2016.
 */

public class DivisionFilterItem {

    private Division mDivision;
    private boolean mSelected;

    public DivisionFilterItem(Division division){
        this(division, false);
    }

    public DivisionFilterItem(Division division, boolean selected){
        mDivision = division;
        mSelected = selected;
    }

    public Division getDivision() {
        return mDivision;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }
}
