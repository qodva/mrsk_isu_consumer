package com.datumsoft.app.mrsk_consumer.models;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by godko on 08.09.2016.
 */
public class ISUCounterListDeserializer implements JsonDeserializer<ArrayList<ISUCounter>> {

    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    private final String TAG = "ISUCounterDeserializer";

    @Override
    public ArrayList<ISUCounter> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ArrayList<ISUCounter> counterCollection = new ArrayList<>();
        JsonObject obj = json.getAsJsonObject();
        JsonArray results = obj.getAsJsonArray("results");
        if (results.size() > 0){
            for (JsonElement el: results){
                counterCollection.add(parseCounter(el));
            }
        }
        return counterCollection;
    }

    private ISUCounter parseCounter(JsonElement json){
        JsonObject obj = json.getAsJsonObject();
        String counterID = obj.get("pk").getAsString();

        ISUCounter counter = new ISUCounter(counterID);
        counter.setDescription(obj.get("__unicode__").getAsString());

        JsonObject fields = obj.getAsJsonObject("fields");
        if (fields.has("consumer")){
            counter.setConsumer(ForeignField.FromJsonObject(fields.getAsJsonObject("consumer")));
        }
        if (fields.has("photo")){
            String photo = fields.get("photo").getAsString();
            counter.setPhotoURI(photo);
        }
        if (fields.has("datetime")) {
            String dateString = fields.get("datetime").getAsString().replace("Z", "+0000");
            try{
                counter.setDateTime(sDateFormat.parse(dateString));
            }
            catch (ParseException e){
                Log.d(TAG, e.getMessage());
            }
        }
        if (fields.has("author")){
            JsonElement authorEl = fields.get("author");
            if (!authorEl.isJsonNull()){
                counter.setAuthor(ForeignField.FromJsonObject(fields.getAsJsonObject("author")));
            }
        }

        if (fields.has("value")){
            counter.setValue(fields.get("value").getAsDouble());
        }

        return counter;
    }
}