package com.datumsoft.app.mrsk_consumer.models;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by godko on 26.09.2016.
 */

public class DivisionGroupFilter implements Serializable{

    public final static String FEATURE_CLASS = "division";
    private final static Gson sGson = new Gson();

    private String mDescription;
    private HashMap<String, Object> mFilter = new HashMap<>();
    private boolean mExpanded;
    private List<DivisionFilterItem> mDivisions = new ArrayList<>();

    public DivisionGroupFilter(String description){
        this(description, null);
    }

    public DivisionGroupFilter(String description, HashMap<String, Object> filter){
        this(description, filter, false);
    }

    public DivisionGroupFilter(String description, HashMap<String, Object> filter, boolean expanded){
        mDescription = description;
        mExpanded = expanded;
        if (filter != null){
            mFilter.putAll(filter);
        }
    }

    public void addFilterOption(String key, Object value){
        if (key != null && value != null){
            mFilter.put(key, value);
        }
    }

    public void removeFilterOption(String key){
        mFilter.remove(key);
    }

    public String getDescription() {
        return mDescription;
    }

    public boolean isExpanded() {
        return mExpanded;
    }

    public void setExpanded(boolean expanded) {
        mExpanded = expanded;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public HashMap<String, Object> getFilter() {
        return mFilter;
    }

    public String getFilterURL() {
        return sGson.toJson(mFilter);
    }

    public List<DivisionFilterItem> getDivisions() {
        return mDivisions;
    }

    public void setDivisions(List<DivisionFilterItem> divisions) {
        for (DivisionFilterItem div: divisions){
            mDivisions.add(div);
        }
    }
}
