package com.datumsoft.app.mrsk_consumer.models;

import android.os.AsyncTask;

import com.datumsoft.app.mrsk_consumer.loaders.FeatureListLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by godko on 26.09.2016.
 */

public class DivisionLoadTask extends AsyncTask<DivisionLoadTask.OnLoadListener, Void, ArrayList<Division>> {

    private static final String FEATURE_CLASS = "division";
    private final String mRestPoint;
    private DivisionGroupFilter mDivisionGroupFilter;
    private List<OnLoadListener> listeners = new ArrayList<>();

    public interface OnLoadListener{
        public void onLoad(ArrayList<Division> divisions);
    }

    public DivisionLoadTask(String restPoint, DivisionGroupFilter divGroupFilter){
        mRestPoint = restPoint;
        mDivisionGroupFilter = divGroupFilter;
    }

    @Override
    protected ArrayList<Division> doInBackground(DivisionLoadTask.OnLoadListener... params) {
        for (OnLoadListener listener: params){
            listeners.add(listener);
        }
        FeatureListLoader<Division> loader = new FeatureListLoader<>(mRestPoint, FEATURE_CLASS,
                Division.class, new DivisionListDeserializer());
        loader.addLoadingField("name");
        loader.addLoadingField("code");
        for(HashMap.Entry<String, Object> filterItem: mDivisionGroupFilter.getFilter().entrySet()){
            loader.addFilterOption(filterItem.getKey(), filterItem.getValue());
        }
        return loader.load();
    }

    @Override
    protected void onPostExecute(ArrayList<Division> divisions) {
        super.onPostExecute(divisions);
        for (OnLoadListener listener: listeners){
            listener.onLoad(divisions);
        }
    }


}

