package com.datumsoft.app.mrsk_consumer;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.datumsoft.app.mrsk_consumer.loaders.FeatureListLoader;
import com.datumsoft.app.mrsk_consumer.models.ISUConsumer;
import com.datumsoft.app.mrsk_consumer.models.ISUConsumerDeserializer;
import com.datumsoft.app.mrsk_consumer.models.ISUConsumerListDeserializer;
import com.github.filosganga.geogson.gson.GeometryAdapterFactory;
import com.github.filosganga.geogson.model.Feature;
import com.github.filosganga.geogson.model.FeatureCollection;
import com.github.filosganga.geogson.model.Point;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import org.osmdroid.api.IMapController;
import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MapActivity extends AppCompatActivity {

    public static int REQUEST_CONSUMER_LIST = 0;
    static final int REQUEST_LOCATION_ACCESS = 0;
    static final String EXTRA_LOGIN = "login";


    final GeoPoint startMapPoint = new GeoPoint(47.2373287, 39.708023);
    final int startZoom = 15;
    final int minZoom = 6;
    final int maxZoom = 20;
    final int consumerZoom = 17;
    final String TAG = "map";

    private LocationManager mLocationManager;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mapController.setCenter(new GeoPoint(location.getLatitude(), location.getLongitude()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    MapView map;
    IMapController mapController;
    private ProgressBar mProgressBar;

    ArrayList<OverlayItem> geoConsumers = new ArrayList<>();
    ItemizedIconOverlay<OverlayItem> consumerOverlay;

    String restPoint;
    int mLoginID;

    public static Intent newIntent(Context context, int loginID){
        Intent intent = new Intent(context, MapActivity.class);
        intent.putExtra(EXTRA_LOGIN, loginID);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initializeMap();

        mLoginID = getIntent().getIntExtra(EXTRA_LOGIN, 0);
        restPoint = getResources().getString(R.string.rest_point);

        map.setMapListener(new DelayedMapListener(new MapListener() {
            @Override
            public boolean onScroll(ScrollEvent event) {
                Log.d(TAG, "scroll");
                redrawMap();
                return false;
            }

            @Override
            public boolean onZoom(ZoomEvent event) {
                Log.d(TAG, "zoom");
                return false;
            }
        }, 100));

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mProgressBar = (ProgressBar) findViewById(R.id.map_progressbar);

    }

    private void redrawMap(){
        GeoConsumerLoaderTask task = new GeoConsumerLoaderTask(this);
        new GeoConsumerLoaderTask(this).execute(restPoint);
    }

    /*
    Initializer mapView options and saving mapView and him controller
    to object variables.
     */
    private void initializeMap(){
        Resources resources = getResources();
        this.map = (MapView) findViewById(R.id.map);

        this.map.setTileSource(TileSourceFactory.MAPNIK);
        this.map.setBuiltInZoomControls(true);
        this.map.setMultiTouchControls(true);
        this.map.setMinZoomLevel(minZoom);
        this.map.setMaxZoomLevel(maxZoom);

        this.mapController = this.map.getController();
        this.mapController.setCenter(startMapPoint);
        this.mapController.setZoom(startZoom);
    }


    /*
    Async task for loading consumer geojson.
     */
    class GeoConsumerLoaderTask extends AsyncTask<String, Void, Void>
    {

        private String url;
        private final String featureClass = "sapisuconsumer";
        private Context context;
        private Map<String, Object> filter = new HashMap<>();
        private Drawable legalMarker;
        private Drawable physicalMarker;

        public GeoConsumerLoaderTask(Context context){
            super();
            this.context = context;
            filter.put("geom__isnull", false);
            legalMarker = loadMarker(R.drawable.isu_legal);
            physicalMarker = loadMarker(R.drawable.isu_physical);
        }

        private Drawable loadMarker(int resId){
            Drawable itemMarker = getResources().getDrawable(resId);
            Bitmap bitmap = ((BitmapDrawable) itemMarker).getBitmap();
            Drawable scaleItemMarker = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 32, 32,true));
            return scaleItemMarker;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {
            try{
                FeatureCollection consumers = loadConsumer(params[0]);
                if (consumers != null){
                    addToMap(consumers);
                }
            }
            catch (IOException e){
                Log.d("map", "Loading error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isCancelled()){
                return;
            }
            map.getOverlays().clear();
            map.invalidate();
            map.getOverlays().add(consumerOverlay);
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);

        }

        protected FeatureCollection loadConsumer(String stringURL) throws IOException{
            String finalString = stringURL + featureClass + "/";
            Log.d("map", finalString);
            HttpURLConnection conn = null;
            FeatureCollection collection = null;
            try{
                String dataURL = stringURL + featureClass + "/?format=geojson&filter=" + URLEncoder.encode(getFilterString()) + "&fields=code,geom";
                URL url = new URL(dataURL);
                Log.d(TAG, url.toString());
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setReadTimeout(100 * 1000);
                conn.connect();

                Gson gson = new GsonBuilder()
                        .registerTypeAdapterFactory(new GeometryAdapterFactory())
                        .create();

                JsonReader reader = new JsonReader(new InputStreamReader(conn.getInputStream()));
                collection = gson.fromJson(reader, FeatureCollection.class);
                Log.d("map", "Features Count " + collection.features().size());
            }
            catch (Exception e){
                Log.d("map", e.toString());
            }
            finally {
                if (conn != null){
                    conn.disconnect();
                }
            }
            return collection;
        }

        private String getFilterString(){
            Gson gson = new Gson();
            Object x = filter.remove("geom__intersects");
            filter.put("geom__intersects", Utils.BboxToWkt(map.getBoundingBox()));
            return gson.toJson(filter);
        }

        private void addFilterOption(String key, Object value){
            filter.put(key, value);
        }

        protected void addToMap(FeatureCollection ftrCollection){
            Log.d("map", "Add to map");
            geoConsumers.clear();
            for(Feature ftr: ftrCollection.features()){
                Point geom = (Point) ftr.geometry();
                OverlayItem item = new OverlayItem(ftr.properties().get("code").getAsString(), null, null, new GeoPoint(geom.lat(), geom.lon()));
                item.setMarker(legalMarker);
                geoConsumers.add(item);
            }
            consumerOverlay = new ItemizedIconOverlay<>(geoConsumers, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                @Override
                public boolean onItemSingleTapUp(int index, OverlayItem item) {
                    Log.d("map", item.getUid());
                    new ConsumerLoadTask(context, restPoint).execute(item.getUid());
                    // Add showing action.
                    return false;
                }

                @Override
                public boolean onItemLongPress(int index, OverlayItem item) {
                    return false;
                }
            }, context);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(mLocationListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_ACCESS);
        }
        else {
            if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        1000 * 10, 100, mLocationListener);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = ConsumerListActivity.newIntent(this, map.getBoundingBox(), mLoginID);
        startActivityForResult(intent, REQUEST_CONSUMER_LIST);
//        new ConsumerListTask(this, map.getBoundingBox()).execute();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CONSUMER_LIST &&
                resultCode == RESULT_OK){
            mapController.setZoom(consumerZoom);
            mapController.setCenter((GeoPoint) data.getSerializableExtra(ConsumerListActivity.EXTRA_CONSUMER_POINT));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

class ConsumerLoadTask extends AsyncTask<String, Void, Void>
{
    private String restPoint;
    private Context context;
    private final String featureClass = "sapisuconsumer";
    private final String TAG = "map";
    private ISUConsumer consumer;

    public ConsumerLoadTask(Context context, String restPoint){
        this.restPoint = restPoint;
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... params) {
        if (params.length == 0) {
            Log.d("map", "Empty params");
            return null;
        }
        String consumerCode = params[0];
        consumer = loadConsumer(consumerCode);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Intent intent = new Intent(context, ConsumerActivity.class);
        intent.putExtra("consumer", consumer);
        context.startActivity(intent);
    }

    private ISUConsumer loadConsumer(String code){
        ISUConsumer consumer = null;
        HttpURLConnection conn = null;
        try{
            Log.d(TAG, restPoint + featureClass + "/" + code + "/");
            conn = (HttpURLConnection) new URL(restPoint + featureClass + "/" + code + "/").openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(ISUConsumer.class, new ISUConsumerDeserializer())
                    .create();
            JsonReader reader = new JsonReader(new InputStreamReader(conn.getInputStream()));
            consumer = gson.fromJson(reader, ISUConsumer.class);
        }
        catch(IOException e){
            Log.e(TAG, "Error on loading consumer data");
        }
        finally {
            if (conn != null){
                conn.disconnect();
            }
        }
        return consumer;
    }

}
