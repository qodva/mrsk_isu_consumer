package com.datumsoft.app.mrsk_consumer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.datumsoft.app.mrsk_consumer.models.ISUCounter;
import com.datumsoft.app.mrsk_consumer.models.ISUCounterDeserializer;
import com.datumsoft.app.mrsk_consumer.models.ISUCounterListDeserializer;
import com.datumsoft.app.mrsk_consumer.utils.MultipartUtility;
import com.datumsoft.app.mrsk_consumer.utils.PictureUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class CreateCounterActivity extends AppCompatActivity {


    public static final String EXTRA_COUNTER = "Counter";
    private static final String TAG = "CreateCounter";
    private static final String EXTRA_PHOTO_FILE =
            "CreateCounterActivity.photo_file";
    private static final String EXTRA_CONSUMER_CODE =
            "ConsumerCode";
    private static final String EXTRA_LOGIN =
            "LOGIN";

    private static final int REQUEST_PHOTO = 0;

    private ImageView mPhotoView;
    private EditText mCounterValueView;

    private File mCounterPhoto;
    private Double mCounterValue;
    private Intent mPhotoIntent;
    private String mConsumerCode;
    private String mAuthor;

    private double mLatitude;
    private double mLongitude;
    private int mLoginID;

    private LocationManager mLocationManager;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_counter_acitivity);

        mPhotoView = (ImageView) findViewById(R.id.counter_create_image_view);
        mCounterValueView = (EditText) findViewById(R.id.counter_value_edit_text);

        mPhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mConsumerCode = getIntent().getStringExtra(EXTRA_CONSUMER_CODE);
        mLoginID = getIntent().getIntExtra(EXTRA_LOGIN, 0);
        if (savedInstanceState != null){
            mCounterPhoto = (File) savedInstanceState.getSerializable(EXTRA_PHOTO_FILE);
            updatePhoto();
        }
        else{
            mCounterPhoto = getPhotoFile();
        }
        mPhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCounterPhoto));
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    public static Intent newIntent(Context context, String consumerCode, int loginID){
        Intent intent = new Intent(context, CreateCounterActivity.class);
        intent.putExtra(EXTRA_CONSUMER_CODE, consumerCode);
        intent.putExtra(EXTRA_LOGIN, loginID);
        return intent;
    }

    public void createCounterImage(View v){
        Log.d(TAG, "CatchImage");
        startActivityForResult(mPhotoIntent, REQUEST_PHOTO);
    }

    public void saveCounter(View v){
        mCounterValue = Double.parseDouble(mCounterValueView.getText().toString());
        if (validate()){
            Log.d(TAG, "Valid data");
            new PostCounterTask(this, getResources().getString(R.string.rest_point)).execute();
        }
        else{

            Log.d(TAG, "Invalid data");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(EXTRA_PHOTO_FILE, mCounterPhoto);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(mLocationListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)){
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000 * 10, 100, mLocationListener);
        }
    }


    private boolean validate(){
        if (mLatitude == 0 || mLongitude == 0){
            if (!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateCounterActivity.this);
                builder.setTitle("Ошибка")
                    .setMessage("Включите функцию геокодирования")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                builder.create().show();
            }
            Toast.makeText(this, R.string.empty_geom_error, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private File getPhotoFile(){
        File externalFileDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFileDir == null){
            return null;
        }
        return new File(externalFileDir, UUID.randomUUID().toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PHOTO) {
            if (resultCode == RESULT_OK) {
                updatePhoto();
            }
        }
    }
    private void updatePhoto(){

        Bitmap bitMap = BitmapFactory.decodeFile(mCounterPhoto.getPath());
//        Bitmap bitMap = PictureUtils.getScaledBitmap(mCounterPhoto.getPath(), this);
        mPhotoView.setImageBitmap(bitMap);
    }


    private class PostCounterTask extends AsyncTask<Void, Void, ISUCounter>
    {
        private final String FEATURE_CLASS = "SapISUPerformancecounter";
        private final String CHARSET = "UTF-8";
        private String mRestPoint;
        private Context mContext;
        private ProgressDialog mProgressDialog;

        private Gson sGson = new GsonBuilder()
            .registerTypeAdapter(ISUCounter.class, new ISUCounterDeserializer())
            .create();


        public PostCounterTask(Context context, String restPoint){
            super();
            mContext = context;
            mRestPoint = restPoint;
        }

        @Override
        protected ISUCounter doInBackground(Void... params) {
            ISUCounter newCounter = loadCounter();
            return newCounter;
        }

        @Override
        protected void onPostExecute(ISUCounter counter) {
            if (counter == null){
                return;
            }
            Intent dataIntent = new Intent();
            dataIntent.putExtra(EXTRA_COUNTER, counter);
            setResult(RESULT_OK, dataIntent);
            Toast.makeText(mContext, R.string.counter_creation_success, Toast.LENGTH_LONG)
                    .show();
            mProgressDialog.hide();
            finish();
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setTitle(mContext.getResources().getString(R.string.counter_create_title));
            mProgressDialog.setMessage(mContext.getResources().getString(R.string.counter_create_message));
            mProgressDialog.show();
        }

        private ISUCounter loadCounter(){
            HttpURLConnection conn = null;
            ISUCounter counter = null;
            try{
                MultipartUtility multipart = new MultipartUtility(buildURI(), CHARSET);
                multipart.addFormField("author", Integer.toString(mLoginID));
                multipart.addFormField("value", mCounterValue.toString());
                multipart.addFormField("consumer", mConsumerCode);
                multipart.addFormField("geom",  String.format(Locale.ROOT, "POINT(%1$f %2$f)", mLongitude, mLatitude));
                multipart.addFilePart("photo", mCounterPhoto);
                List<String> response = multipart.finish();

                counter = sGson.fromJson(response.get(0), ISUCounter.class);
            }
            catch(NullPointerException e){
                Log.e(TAG, "Null reference exception on reading created counters", e);
            }
            catch(IOException e){
                Log.e(TAG, "Error on loading consumer data");
            }
            finally {
                if (conn != null){
                    conn.disconnect();
                }
            }


            return counter;
        }

        private String buildURI(){
            return mRestPoint + FEATURE_CLASS + "/";
        }


    }
}
