package com.datumsoft.app.mrsk_consumer.loaders;


import android.util.Log;

import com.datumsoft.app.mrsk_consumer.models.ISUConsumer;
import com.datumsoft.app.mrsk_consumer.models.ISUCounter;
import com.datumsoft.app.mrsk_consumer.models.ISUCounterListDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class CounterListLoader {

    private final String FEATURE_CLASS = "SapISUPerformanceCounter";
    private final String TAG = "counterList";
    private String mRestPoint;
    private HashMap<String, Object> mFilter = new HashMap<>();
    private Gson gson;


    public CounterListLoader(String restPoint) {
        mRestPoint = restPoint;
        gson = new GsonBuilder()
                .registerTypeAdapter(ISUCounter.class, new ISUCounterListDeserializer())
                .create();
    }

    public void addFilterOption(String key, Object value){
        mFilter.put(key, value);
    }

    public void removeFilterOption(String key){
        mFilter.remove(key);
    }

    public ArrayList<ISUCounter> load() {
        ISUConsumer consumer = null;
        HttpURLConnection conn = null;
        ArrayList<ISUCounter> counterList = null;
        try{
            Log.d(TAG, mRestPoint + FEATURE_CLASS + "/");
            conn = (HttpURLConnection) new URL(buildURI()).openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            JsonReader reader = new JsonReader(new InputStreamReader(conn.getInputStream()));
            counterList = gson.fromJson(reader, ISUCounter.class);
        }
        catch(IOException e){
            Log.e(TAG, "Error on loading consumer data");
            counterList = new ArrayList<>();
        }
        finally {
            if (conn != null){
                conn.disconnect();
            }
        }
        return counterList;
    }

    private String buildURI(){
//        return mRestPoint + FEATURE_CLASS + "/";
        return mRestPoint + FEATURE_CLASS + "/?filter=" + gson.toJson(mFilter);
    }
}