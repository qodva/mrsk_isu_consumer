package com.datumsoft.app.mrsk_consumer.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by godko on 01.09.2016.
 */
public class ISUConsumerDeserializer implements JsonDeserializer<ISUConsumer> {

    @Override
    public ISUConsumer deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject obj = jsonElement.getAsJsonObject();
        JsonArray results = obj.getAsJsonArray("results");
        if (results.size() == 0){
            return null;
        }
        ISUConsumer consumer = buildConsumer(results.get(0).getAsJsonObject());
        return consumer;
    }

    private ISUConsumer buildConsumer(JsonObject obj){
        ISUConsumer consumer = new ISUConsumer();
        consumer.setDescription(obj.get("__unicode__").getAsString());
        consumer.setCode(obj.get("pk").getAsString());
        JsonObject fields = obj.getAsJsonObject("fields");
        JsonElement el = null;

        consumer.setRsk(ForeignField.FromJsonObject(fields.getAsJsonObject("rsk")));
        consumer.setPes(ForeignField.FromJsonObject(fields.getAsJsonObject("pes")));
        consumer.setRes(ForeignField.FromJsonObject(fields.getAsJsonObject("res")));

        consumer.setKks(fields.get("kks").getAsString());
        consumer.setName(fields.get("name").getAsString());
        consumer.setAddress(fields.get("address").getAsString());
        consumer.setCity(fields.get("city").getAsString());
        consumer.setStreet(fields.get("street").getAsString());
        consumer.setHouse(fields.get("house").getAsString());
        consumer.setSerialNumber(fields.get("serial_number").getAsString());
        if (fields.has("name_contract")){
            el = fields.get("name_contract");
            if (!el.isJsonNull()){
                consumer.setNameContract(fields.get("name_contract").getAsString());
            }
        }
        consumer.setCounterType(fields.get("counter_type").getAsString());
        consumer.setLegal(fields.get("is_legal").getAsBoolean());

        return consumer;
    }
}
