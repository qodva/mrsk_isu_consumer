package com.datumsoft.app.mrsk_consumer;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.models.Division;
import com.datumsoft.app.mrsk_consumer.models.DivisionFilterItem;
import com.datumsoft.app.mrsk_consumer.models.DivisionGroupFilter;
import com.datumsoft.app.mrsk_consumer.models.DivisionLoadTask;
import com.github.aakira.expandablelayout.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by godko on 26.09.2016.
 */

public class DivisionFilterRecyclerAdapter extends RecyclerView.Adapter<DivisionFilterRecyclerAdapter.ViewHolder> {

    private final List<DivisionGroupFilter> mData;
    private Context mContext;

    public static class ViewHolder extends  RecyclerView.ViewHolder
        implements View.OnClickListener
    {
        private TextView mGroupFilterCaptionView;
        private DivisionGroupFilter mDivisionGroupFilter;
        private ExpandableLinearLayout mExpandableLinearLayout;
        private RelativeLayout mButtonLayout;
        private String mRestPoint;
        private RecyclerView mDivisionItemsView;
        private ListView mListView;

        public ViewHolder(View itemView){
            super(itemView);
            mRestPoint = itemView.getResources().getString(R.string.rest_point);
            mGroupFilterCaptionView = (TextView) itemView.findViewById(R.id.group_division_filter_view);
            mExpandableLinearLayout = (ExpandableLinearLayout) itemView.findViewById(R.id.expandableLayout);
            mButtonLayout = (RelativeLayout) itemView.findViewById(R.id.button);
            mDivisionItemsView = (RecyclerView) itemView.findViewById(R.id.division_items);
            mListView = (ListView) itemView.findViewById(R.id.filter_list_view);
        }

        public void setGroupFilter(DivisionGroupFilter divisionGroupFilter){
            mDivisionGroupFilter = divisionGroupFilter;
            mGroupFilterCaptionView.setText(mDivisionGroupFilter.getDescription());
            mExpandableLinearLayout.setInRecyclerView(true);
            mExpandableLinearLayout.setExpanded(mDivisionGroupFilter.isExpanded());

            mExpandableLinearLayout.setListener(new ExpandableLayoutListenerAdapter() {
                @Override
                public void onPreClose() {
                    mDivisionGroupFilter.setExpanded(false);
                    createRotateAnimator(mButtonLayout, 180f, 0f).start();
                }

                @Override
                public void onPreOpen() {
                    mDivisionGroupFilter.setExpanded(true);
                    createRotateAnimator(mButtonLayout, 0f, 180f).start();

                    // If divisions elements not loaded.
                    if (mDivisionGroupFilter.getDivisions().size() == 0){
                        new DivisionLoadTask(mRestPoint, mDivisionGroupFilter).execute(new DivisionOnLoadListener());
                    }
                }
            });
            mButtonLayout.setRotation(mDivisionGroupFilter.isExpanded() ? 180f : 0f);
            mButtonLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mExpandableLinearLayout.toggle();
                }
            });

            mGroupFilterCaptionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mExpandableLinearLayout.toggle();
                }
            });
        }

        @Override
        public void onClick(View v) {
            mExpandableLinearLayout.toggle();
        }

        private static ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
            animator.setDuration(300);
            animator.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_INTERPOLATOR));
            return animator;
        }

        class DivisionOnLoadListener implements DivisionLoadTask.OnLoadListener{
            @Override
            public void onLoad(ArrayList<Division> divisions) {
                ArrayList<DivisionFilterItem> divisionFilterItems = new ArrayList();
                for (Division div: divisions){
                    divisionFilterItems.add(new DivisionFilterItem(div));
                }
                mDivisionGroupFilter.getDivisions().addAll(divisionFilterItems);
                mDivisionItemsView.setAdapter(new DivisionItemsFilterAdapter(divisionFilterItems));
                mDivisionItemsView.setLayoutManager(new AwesomeLayoutManager(mDivisionItemsView));
//                mDivisionItemsView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
//                mDivisionItemsView.setLayoutManager(new CustomLinearLayoutManager(itemView.getContext()));


                List<String> names = new ArrayList<>();
                for (Division div: divisions){
                    names.add(div.getName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(itemView.getContext(), android.R.layout.simple_list_item_1, names);
                mListView.setAdapter(adapter);
            }
        }
    }


    public DivisionFilterRecyclerAdapter(List<DivisionGroupFilter> data){
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.activity_division_filter_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setGroupFilter(mData.get(position));
        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}