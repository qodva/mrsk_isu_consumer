package com.datumsoft.app.mrsk_consumer.loaders;

import android.text.TextUtils;
import android.util.Log;

import com.datumsoft.app.mrsk_consumer.models.ISUConsumer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by godko on 16.09.2016.
 */
public class FeatureListLoader<T extends Object> {

    private final String TAG = "FeatureListLoader";
    private final String ENCODING = "UTF-8";
    private String mFeatureClass = "SapISUPerformanceCounter";
    private String mRestPoint;
    private HashMap<String, Object> mFilter = new HashMap<>();
    private Gson gson;
    private Class<T> mModelClass;
    private ArrayList<String> mLoadingFields = new ArrayList<>();


    public FeatureListLoader(String restPoint, String featureClass,
                             Class<T> modelClass, JsonDeserializer<ArrayList<T>> deserializer) {
        mRestPoint = restPoint;
        mFeatureClass = featureClass;
        mModelClass = modelClass;
        gson = new GsonBuilder()
                .registerTypeAdapter(modelClass, deserializer)
                .create();
    }

    public void addFilterOption(String key, Object value){
        mFilter.put(key, value);
    }

    public void removeFilterOption(String key){
        mFilter.remove(key);
    }

    public ArrayList<T> load() {
        ISUConsumer consumer = null;
        HttpURLConnection conn = null;
        ArrayList<T> counterList = null;
        try{
            Log.d(TAG, mRestPoint + mFeatureClass + "/");
            CookieManager manager = new CookieManager();
            conn = (HttpURLConnection) new URL(buildURI()).openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            JsonReader reader = new JsonReader(new InputStreamReader(conn.getInputStream()));
            counterList = gson.fromJson(reader, mModelClass);
        }
        catch(IOException e){
            Log.e(TAG, "Error on loading consumer data");
            counterList = new ArrayList<T>();
        }
        finally {
            if (conn != null){
                conn.disconnect();
            }
        }
        return counterList;
    }

    private String buildURI() throws UnsupportedEncodingException{
        String featureURL = mRestPoint + mFeatureClass + "/?filter=" + URLEncoder.encode(gson.toJson(mFilter), ENCODING);
        if (mLoadingFields.size() != 0){
            featureURL += "&fields=" + TextUtils.join(",", mLoadingFields);
        }
        return featureURL;
    }


    public void addLoadingField(String field){
        mLoadingFields.add(field);
    }

    public void removeLoadingField(String field){
        mLoadingFields.remove(field);
    }
}