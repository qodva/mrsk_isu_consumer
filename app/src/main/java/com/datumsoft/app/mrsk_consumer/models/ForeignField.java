package com.datumsoft.app.mrsk_consumer.models;

import com.google.gson.JsonObject;

import java.io.Serializable;

public class ForeignField implements Serializable {

    private String code = "";
    private String description = "";

    public ForeignField(){

    }

    public static ForeignField FromJsonObject(JsonObject obj){
        ForeignField foreignField = new ForeignField();
        if (obj != null && !obj.get("db").isJsonNull()){
            String description = obj.get("human").getAsString();
            String code = obj.get("db").getAsString();
            foreignField.setDescription(description);
            foreignField.setCode(code);
        }
        return foreignField;
    }

    public ForeignField(String code, String description){
        this.code = code;
        this.description = description;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}