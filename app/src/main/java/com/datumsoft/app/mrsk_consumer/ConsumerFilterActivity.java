package com.datumsoft.app.mrsk_consumer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.datumsoft.app.mrsk_consumer.models.DivisionGroupFilter;
import com.datumsoft.app.mrsk_consumer.utils.DividerItemDecoration;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import java.util.ArrayList;
import java.util.List;

public class ConsumerFilterActivity extends AppCompatActivity {

    private static final String EXTRA_LOGIN =
            "LOGIN";

    private int mUserID;
    private static List<DivisionGroupFilter> sDivisionGroupFilter;
    static {
        sDivisionGroupFilter = new ArrayList<>();
        DivisionGroupFilter filial = new DivisionGroupFilter("Филиал");
        filial.addFilterOption("type", "filial");
        sDivisionGroupFilter.add(filial);

        DivisionGroupFilter po = new DivisionGroupFilter("ПО");
        po.addFilterOption("type", "department");
        sDivisionGroupFilter.add(po);

        DivisionGroupFilter res = new DivisionGroupFilter("РЭС");
        res.addFilterOption("type", "district");
        sDivisionGroupFilter.add(res);
    }

    private RecyclerView mDivisionView;

    public static Intent newIntent(Context context, int userID){
        Intent intent = new Intent(context, ConsumerFilterActivity.class);
        intent.putExtra(EXTRA_LOGIN, userID);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumer_filter);

        mUserID = getIntent().getIntExtra(EXTRA_LOGIN, 0);

        mDivisionView = (RecyclerView) findViewById(R.id.division_filter_view);
        mDivisionView.setAdapter(new DivisionFilterRecyclerAdapter(sDivisionGroupFilter));
        mDivisionView.addItemDecoration(new DividerItemDecoration(this));
        mDivisionView.setLayoutManager(new LinearLayoutManager(this));
    }
}
