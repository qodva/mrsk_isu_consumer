package com.datumsoft.app.mrsk_consumer.models;

import java.io.Serializable;

/**
 * Created by godko on 26.09.2016.
 */

public class Division implements Serializable {

    private final String mCode;
    private String mName;


    public Division(String code){
        mCode = code;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
