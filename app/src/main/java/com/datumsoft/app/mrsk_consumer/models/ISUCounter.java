package com.datumsoft.app.mrsk_consumer.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by godko on 08.09.2016.
 */
public class ISUCounter implements Serializable {

    private String mId;

    private String mPhotoURI;
    private Date mDateTime;
    private String mDescription;
    private Double mValue;
    private ForeignField mConsumer = new ForeignField();
    private ForeignField mAuthor = new ForeignField();

    public ISUCounter(String id){
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public ForeignField getConsumer() {
        return mConsumer;
    }

    public void setConsumer(ForeignField consumer) {
        mConsumer = consumer;
    }

    public String getPhotoURI() {
        return mPhotoURI;
    }

    public void setPhotoURI(String photoURI) {
        mPhotoURI = photoURI;
    }

    public Date getDateTime() {
        return mDateTime;
    }

    public void setDateTime(Date dateTime) {
        mDateTime = dateTime;
    }

    public ForeignField getAuthor() {
        return mAuthor;
    }

    public void setAuthor(ForeignField author) {
        mAuthor = author;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Double getValue() {
        return mValue;
    }

    public void setValue(Double value) {
        mValue = value;
    }
}
