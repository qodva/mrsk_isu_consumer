package com.datumsoft.app.mrsk_consumer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.models.ISUCounter;

import java.net.URL;
import java.text.SimpleDateFormat;

public class CounterActivity extends AppCompatActivity {

    private static final String EXTRA_COUNTER = "counter";
    private static final String BUNDLE_DRAWABLE = "counter_drawable";

    private ISUCounter mCounter;
    private ImageView mCounterImageView;
    private TableLayout mCounterAttributesTableView;
    private ProgressBar mLoadImageProgressBar;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);

        mCounter = (ISUCounter) getIntent().getSerializableExtra(EXTRA_COUNTER);

        mCounterAttributesTableView = (TableLayout) findViewById(R.id.counter_attribute_table);
        updateTable();

        mLoadImageProgressBar = (ProgressBar) findViewById(R.id.counter_image_load_progressbar);

        mCounterImageView = (ImageView) findViewById(R.id.counter_image_view);
        new ImageLoadingTask(mCounterImageView, mLoadImageProgressBar)
                .execute(buildMediaURI(mCounter.getPhotoURI()));

    }

    public static Intent newIntent(Context context, ISUCounter counter){
        Intent intent = new Intent(context, CounterActivity.class);
        intent.putExtra(EXTRA_COUNTER, counter);
        return intent;
    }

    public String buildMediaURI(String uri){
        return getResources().getString(R.string.media_point) + uri;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //TODO: add saving drawable in bundle and loading
        //him on change system state.
        super.onSaveInstanceState(outState);
    }

    protected void updateTable(){
        mCounterAttributesTableView.addView(getTableRow("Значение счетчика", mCounter.getValue().toString()));
        mCounterAttributesTableView.addView(getTableRow("Дата фиксирования", mDateFormat.format(mCounter.getDateTime())));
        mCounterAttributesTableView.addView(getTableRow("Обходчик", mCounter.getAuthor().getDescription()));
    }

    private TableRow getTableRow(String caption, String value){
        LayoutInflater inflater = LayoutInflater.from(this);
        TableRow row = (TableRow) inflater.inflate(R.layout.fragment_attribute_table_row, null);
        ((TextView) row.findViewById(R.id.caption_row_attr)).setText(caption);
        ((TextView) row.findViewById(R.id.value_row_attr)).setText(value);

        return row;
    }

    public void clickCounterImage(View v){
    //        Intent intent = new Intent(this, CounterImageActivity.class);
    //        startActivity(intent);
    //        Drawable viewDrawable = mCounterImageView.getDrawable();
    //        if (viewDrawable != null){
    //
    //            BitmapDrawable dr = (BitmapDrawable) viewDrawable;
    //            Intent intent = CounterImageActivity.newIntent(this, dr.getBitmap());
    //            startActivity(intent);
    //        }
    }

}


class ImageLoadingTask extends AsyncTask<String, Void, Drawable>
{

    private ImageView mImageView;
    private ProgressBar mProgressBar;

    public ImageLoadingTask(ImageView imageView, ProgressBar progressBar){
        mImageView = imageView;
        mProgressBar = progressBar;
    }


    @Override
    protected Drawable doInBackground(String... params) {
        if (params.length != 0){
            return loadImage(params[0]);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Drawable drawable) {
        super.onPostExecute(drawable);
        if (drawable != null){
            mImageView.setImageDrawable(drawable);
            mImageView.invalidate();
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);
        }
    }

    private Drawable loadImage(String uri){
        Drawable thumb = null;
        try{
            URL thumbURL = new URL(uri);
            thumb = Drawable.createFromStream(thumbURL.openStream(), "src");
        }
        catch (Exception e){
            Log.d("error", "loading image", e);
        }
        return thumb;
    }

}