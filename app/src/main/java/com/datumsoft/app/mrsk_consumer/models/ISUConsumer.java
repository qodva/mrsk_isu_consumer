package com.datumsoft.app.mrsk_consumer.models;

import com.google.gson.JsonObject;

import org.osmdroid.util.GeoPoint;

import java.io.Serializable;

public class ISUConsumer implements Serializable{
    public static String FEATURE_CLASS = "SapISUConsumer";

    private String description;
    private ForeignField rsk;
    private ForeignField pes;
    private ForeignField res;
    private String kks;
    private String name;
    private String address;
    private String city;
    private String street;
    private String house;
    private String code;
    private String nameContract;
    private String serialNumber;
    private String counterType;
    private boolean isLegal;
    private GeoPoint mPoint;

    public GeoPoint getPoint() {
        return mPoint;
    }

    public void setPoint(GeoPoint point) {
        mPoint = point;
    }

    public boolean getLegal() {
        return isLegal;
    }

    public void setLegal(Boolean legal) {
        isLegal = legal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ForeignField getRes() {
        return res;
    }

    public void setRes(ForeignField res) {
        this.res = res;
    }

    public ForeignField getPes() {
        return pes;
    }

    public void setPes(ForeignField pes) {
        this.pes = pes;
    }

    public ForeignField getRsk() {
        return rsk;
    }

    public void setRsk(ForeignField rsk) {
        this.rsk = rsk;
    }

    public String getKks() {
        return kks;
    }

    public void setKks(String kks) {
        this.kks = kks;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCounterType() {
        return counterType;
    }

    public void setCounterType(String counterType) {
        this.counterType = counterType;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameContract() {
        return nameContract;
    }

    public void setNameContract(String nameContract) {
        this.nameContract = nameContract;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public boolean hasGeom(){
        return mPoint != null;
    }

}
