package com.datumsoft.app.mrsk_consumer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.models.ForeignField;
import com.datumsoft.app.mrsk_consumer.models.ISUCounter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CounterListActivity extends AppCompatActivity {

    private static int REQUEST_CREATE_COUNTER_CODE = 0;
    private static String EXTRA_COUNTER_LIST = "CounterList";
    private static String EXTRA_CONSUMER_CODE = "ConsumerCode";
    private static final String EXTRA_LOGIN = "LOGIN";

    public ArrayList<ISUCounter> mCounterList;
    private RecyclerView mCounterListView;
    private CounterListAdapter mCounterAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String mConsumerCode;
    private TextView mEmptyListTextView;

    private int mLoginID;

    private final RecyclerView.AdapterDataObserver mObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            checkEmpty();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            checkEmpty();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            super.onItemRangeChanged(positionStart, itemCount, payload);
            checkEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            checkEmpty();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            checkEmpty();
        }

        private void checkEmpty(){
            if (mCounterList.size() == 0){
                mEmptyListTextView.setVisibility(ProgressBar.VISIBLE);
            }
            else {
                mEmptyListTextView.setVisibility(ProgressBar.GONE);
            }
        }

    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter_list);

        mCounterList = (ArrayList<ISUCounter>) getIntent().getSerializableExtra(EXTRA_COUNTER_LIST);

        mLoginID = getIntent().getIntExtra(EXTRA_LOGIN, 0);
        mConsumerCode = getIntent().getStringExtra(EXTRA_CONSUMER_CODE);
        mCounterListView = (RecyclerView) findViewById(R.id.counter_list_view);
        mEmptyListTextView = (TextView) findViewById(R.id.empty_counter_list_text_view);


        mLayoutManager = new LinearLayoutManager(this);
        mCounterListView.setLayoutManager(mLayoutManager);
        mCounterAdapter = new CounterListAdapter(mCounterList);
        mCounterAdapter.registerAdapterDataObserver(mObserver);
        mObserver.onChanged();
        mCounterListView.setAdapter(mCounterAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fragment_counter_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_item_new_counter_data:
                Intent intent = CreateCounterActivity.newIntent(CounterListActivity.this, mConsumerCode, mLoginID);
                startActivityForResult(intent, REQUEST_CREATE_COUNTER_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CREATE_COUNTER_CODE &&
                resultCode == RESULT_OK){
            ISUCounter newCounter = (ISUCounter) data.getSerializableExtra(CreateCounterActivity.EXTRA_COUNTER);
            mCounterList.add(0, newCounter);
            mCounterListView.getAdapter().notifyItemChanged(0);

        }
    }

    public static Intent newIntent(Context context, ArrayList<ISUCounter> counterList,
                                   String consumerCode, int loginID){
        Intent intent = new Intent(context, CounterListActivity.class);
        intent.putExtra(EXTRA_COUNTER_LIST, counterList);
        intent.putExtra(EXTRA_CONSUMER_CODE, consumerCode);
        intent.putExtra(EXTRA_LOGIN, loginID);
        return intent;
    }
}


class CounterListAdapter extends RecyclerView.Adapter<CounterListAdapter.ViewHolder>
{
    private List<ISUCounter> mCounterDataset;

    public static  class ViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener
    {
        private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        private TextView mDateView;
        private TextView mValueView;
        private TextView mAuthorView;
        private ISUCounter mCounter;
        private Context mContext;

        public ViewHolder(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);
            mContext = itemView.getContext();
            mDateView = (TextView)itemView.findViewById(R.id.list_date_view);
            mValueView = (TextView)itemView.findViewById(R.id.list_value_view);
            mAuthorView = (TextView)itemView.findViewById(R.id.list_author_view);
        }

        public void setCounter(ISUCounter counter){
            mCounter = counter;
            setDate(counter.getDateTime());
            setValue(counter.getValue());
            setAuthor(counter.getAuthor().getDescription());
        }

        private void setDate(Date date){
            mDateView.setText(sDateFormat.format(date));
        }

        private void setValue(Double value){
            mValueView.setText("Показания: " + value.toString());
        }

        private void setAuthor(String author){
            mAuthorView.setText("Обходчик: " + author);
        }

        @Override
        public void onClick(View v) {
            Intent intent = CounterActivity.newIntent(mContext, mCounter);
            mContext.startActivity(intent);
        }
    }

    public CounterListAdapter(List<ISUCounter> counterDataset){
        mCounterDataset = counterDataset;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_counter_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setCounter(mCounterDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mCounterDataset.size();
    }
}