package com.datumsoft.app.mrsk_consumer.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by godko on 26.09.2016.
 */

public class DivisionListDeserializer implements JsonDeserializer<ArrayList<Division>> {
    @Override
    public ArrayList<Division> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ArrayList<Division> divisions = new ArrayList<>();
        JsonObject obj = json.getAsJsonObject();
        JsonArray results = obj.getAsJsonArray("results");
        if (results.size() > 0){
            for (JsonElement el: results){
                divisions.add(parseDivision(el.getAsJsonObject()));
            }
        }
        return divisions;
    }

    public Division parseDivision(JsonObject obj){
        JsonObject fields = obj.getAsJsonObject("fields");
        Division division = new Division(fields.get("code").getAsString());
        division.setName(fields.get("name").getAsString());
        return division;
    }
}
