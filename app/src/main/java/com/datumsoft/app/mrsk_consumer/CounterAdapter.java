package com.datumsoft.app.mrsk_consumer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.models.ISUCounter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by godko on 09.09.2016.
 */
public class CounterAdapter extends BaseAdapter {

    private ArrayList<ISUCounter> mCounters;
    private Context mContext;
    private LayoutInflater mInlater;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

    public CounterAdapter(Context context, ArrayList<ISUCounter> counters){
        mCounters = counters;
        mContext = context;
        mInlater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mCounters.size();
    }

    @Override
    public Object getItem(int position) {
        return mCounters.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInlater.inflate(R.layout.adapter_counter_item, parent, false);
        }
        ISUCounter counter = (ISUCounter)getItem(position);
        ((TextView)view.findViewById(R.id.list_date_view)).setText(mDateFormat.format(counter.getDateTime()));
        ((TextView)view.findViewById(R.id.list_value_view)).setText("Показания: " + counter.getValue().toString());
        ((TextView)view.findViewById(R.id.list_author_view)).setText("Обходчик: " + counter.getAuthor());
        return view;
    }
}
