package com.datumsoft.app.mrsk_consumer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.datumsoft.app.mrsk_consumer.models.DivisionFilterItem;

import java.util.List;

/**
 * Created by godko on 26.09.2016.
 */

public class DivisionItemsFilterAdapter extends RecyclerView.Adapter<DivisionItemsFilterAdapter.ViewHolder> {

    private List<DivisionFilterItem> mDivisionsItems;

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private DivisionFilterItem mDivisionFilterItem;
        private TextView mDivisionCaptionView;
        private CheckBox mDivisionCb;

        public ViewHolder(View itemView){
            super(itemView);
            mDivisionCaptionView = (TextView) itemView.findViewById(R.id.division_caption);
            mDivisionCb = (CheckBox) itemView.findViewById(R.id.division_cb);

            mDivisionCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mDivisionFilterItem != null){
                        mDivisionFilterItem.setSelected(isChecked);
                    }
                }
            });
        }

        public void setDivisionItem(DivisionFilterItem divisionFilterItem){
            mDivisionFilterItem = divisionFilterItem;
            mDivisionCaptionView.setText(mDivisionFilterItem.getDivision().getName());
            mDivisionCb.setChecked(mDivisionFilterItem.isSelected());
        }

    }

    public DivisionItemsFilterAdapter(List<DivisionFilterItem> divisionItems){
        mDivisionsItems = divisionItems;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_division_select_item_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setDivisionItem(mDivisionsItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mDivisionsItems.size();
    }
}
