package com.datumsoft.app.mrsk_consumer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class CounterImageActivity extends AppCompatActivity {

    private static final String EXTRA_COUNTER_BITMAP =
            "COUNTER_DRAWABLE";

    private Bitmap mBitmap;
    private ImageView mImageView;
    private View mContentView;

    public static Intent newIntent(Context context, Bitmap image){
        Intent intent = new Intent(context, CounterImageActivity.class);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        intent.putExtra(EXTRA_COUNTER_BITMAP, stream.toByteArray());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_counter_image);



        mContentView = findViewById(R.id.activity_counter_image);
        byte[] bitmapBytes = getIntent().getByteArrayExtra(EXTRA_COUNTER_BITMAP);
        mBitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
        mImageView = (ImageView) findViewById(R.id.counter_image_fullscreen_view);

        mImageView.setImageBitmap(mBitmap);
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );

    }


}
